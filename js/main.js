var ctx,dst,vol;
var kick,snare,hh;
var buffers=[];

var patternLength=16;

function bufferFromFile(f,cb){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', f, true);
    xhr.responseType = 'arraybuffer';

    var buffer;
    
    xhr.onload = function(e) {
        buffer=ctx.createBuffer(xhr.response,false);
        cb(buffer);
    };
    
    xhr.send();
}

$('document').ready(function(){
    var $pattern=$('#pattern');
    var pins=[];
    for(var i=0;i<patternLength;i++){
        var $col=$('<div>',{class:'col'});
        pins.push([false,false,false]);
        for(var j=0;j<4;j++){
            var $p=$('<div>',{class:'pin'});
            $col.append($p);
            (function(pins,i,j){
                $p.click(function(){
                    pins[i][j]=!pins[i][j];
                    //console.log(pins);
                    $(this).toggleClass('active');
                });
            })(pins,i,j);
        }
        $pattern.append($col);
    }

	ctx=new (window.AudioContext||window.webkitAudioContext)();
    dst=ctx.destination;
    vol=ctx.createGain();

    vol.gain.value=0.5;
    
    var playOsc=function(t){
        osc=ctx.createOscillator();
        osc.connect(vol);
        osc.frequency.value=100;
        osc.type=3;
        
        osc.noteOn(ctx.currentTime+t);
        osc.noteOff(ctx.currentTime+t+0.2);
    }
    
    vol.connect(dst);

    bufferFromFile('kick.wav',function(b){
        b.id='kick';
        buffers[0]=b;
    });
    
    bufferFromFile('snare.wav',function(b){
        b.id='snare';
        buffers[2]=b;
    });

    bufferFromFile('hh.wav',function(b){
        b.id='hh';
        buffers[1]=b;
    });

    bufferFromFile('bass.wav',function(b){
        b.id='bass';
        buffers[3]=b;
    });

    play=function(b,t){
        var bs=ctx.createBufferSource();
        bs.buffer=b;
        bs.connect(vol);
        bs.noteOn(t);
    };
 
    var br=140;
    var bt=(60.0/br);
    var pt=bt/4;

    var timer;
    var noteTime;
    var startTime;
    var rythmIndex;
    
    function advanceNote(){
        rythmIndex++;
        if(rythmIndex==patternLength)rythmIndex=0;
        noteTime+=pt;
    }
    
    function movePlayhead(){
        $('.col').toggleClass('current',false);
        $pattern.children().eq(rythmIndex).toggleClass('current',true);  
    }
    
    function sch(){
        var currentTime = ctx.currentTime-startTime;
        while(noteTime<=currentTime+0.001){
            var cpt=noteTime+startTime;
            var current=pins[rythmIndex];

            for(var i=0; i<current.length;i++)if(current[i])play(buffers[i],cpt);

            advanceNote();
            movePlayhead();
        }
        
        timer=setTimeout(sch,0);
    }
    
    $('#play').click(function(){
        clearTimeout(timer);
        rythmIndex=0;
        noteTime=0.0;
        startTime=ctx.currentTime + 0.005;
        sch();
        
    });
    
});
